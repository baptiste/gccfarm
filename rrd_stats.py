from __future__ import division, print_function

import itertools
import math
import os

import rrdtool


MUNIN_PATH = "/var/lib/munin/gccfarm/"
DISKS = ['mmcblk0', 'nvme0n1', 'nvme1n1',
         'vdiska', 'vdiskb', 'vdiskc', # SUN LDOM
         'sda', 'sdb', 'sdc', 'sdd', 'sde', 'sdf', 'sdg', 'sdh', 'sdi', 'sdj', 'sdk', 'sdl']

def mean(l):
    return float(sum(l)) / max(len(l), 1)

def all_none(l):
    return all([x==None for x in l])

# Apply the given function to the given list, filtering None values in the list.
# If all values are None, returns None.
def map_except_none(func, l):
    if all_none(l):
        return
    return func([x for x in l if x != None])

# Like sum(), but discards None values.  If all values are None,
# returns None.
def sum_none(l):
    return map_except_none(sum, l)

# Like max(), but discards None values.  If all values are None,
# returns None.
def max_none(l):
    return map_except_none(max, l)

# Given a list of comparable values and a value p between 0 and 1,
# returns the [p]th quantile of the data.  An optional sort key
# function can be provided.
def quantile(l, p, sort_key=None):
    if len(l) == 0:
        return
    if sort_key != None:
        sorted_data = sorted(l, key=sort_key)
    else:
        sorted_data = sorted(l)
    return sorted_data[int(round((len(l)-1) * p))]

# Compute the distribution of the input list, and apply an exponential
# weight to obtain a weighted average.  The idea is to give more weight
# to high values, for instance:
#
# - a machine 100% busy for 20% of the time will get a weighted average of 63.8%
# - a machine 50% busy for 40% of the time will get a weighted average of 43.6%
# - a machine 20% busy for 100% of the time will get a weighted average of 20%
#
# Intuitively, if a user wants to run a job, using a machine that is
# already 100% busy (even only 20% of the time) is generally a bad
# idea: the new job might significantly interfere with the existing
# ones.  In contrast, a machine that is 20% busy 24/7 still has a lot
# of room for more jobs.

# Alpha controls how much of the weight should be concentrated on the
# high quantiles (higher alpha = more concentrated)
def weighted_average(l, sort_key=None, alpha=5):
    n = len(l)
    if n == 0:
        return
    if sort_key != None:
        sorted_data = sorted(l, key=sort_key)
    else:
        sorted_data = sorted(l)
    # Old linear method
    #weights = [2*k/(n*(n-1)) for k in range(n)]
    # New exponential method
    raw_weights = [math.exp(alpha*(k/(n-1) - 1)) for k in range(n)]
    sum_weights = sum(raw_weights)
    # Normalize weights
    weights = [w/sum_weights for w in raw_weights]
    return sum([w*x for (w, x) in zip(weights, sorted_data)])


# Determines the appropriate memory fields to take into account from Munin.
# Handles Linux and OpenBSD.
def memory_fields(machine):
    linux_fields = ['apps', 'swap', 'page_tables', 'swap_cache', 'shmem']
    # "active" is typically very low.  We take into account "inactive" as well.
    # htop also (indirectly) counts "inactive" memory as being used by applications.
    # OpenBSD man for inactive: "number of pages that we free'd but may want back"
    openbsd_fields = ['active', 'inactive', 'swap', 'wired']
    # Check which RRD exists
    linux_test_rrd = os.path.join(MUNIN_PATH, "{}-memory-apps-g.rrd".format(machine))
    if os.path.exists(linux_test_rrd):
        return linux_fields
    else:
        return openbsd_fields

# Determines the appropriate CPU fields to take into account from Munin.
# Handles Linux and OpenBSD.
def cpu_fields(machine):
    # Just return all known fields from both Linux and OpenBSD.
    # All other functions ignore non-existent fields so it's fine.
    all_fields = ['system', 'user', 'nice', 'iowait', 'irq', 'softirq', 'interrupt', 'steal', 'guest', 'spin']
    return all_fields


# Given a RRD file, fetch the average of the metric (excluding None)
def fetch_avg(machine, rrd, since="-2d"):
    rrd_path = os.path.join(MUNIN_PATH, "{}-{}".format(machine, rrd))
    try:
        res = rrdtool.fetch(rrd_path, "AVERAGE", ["-s", "{}".format(since)])
    except rrdtool.OperationalError:
        return
    return mean([v[0] for v in res[2] if v[0] != None])

# Given a list of RRD files, sum the metrics for each time interval,
# and compute a quantile of the resulting serie of sums.
def fetch_quantile(machine, rrd_list, q="0.95", since="-2d"):
    data = list()
    for rrd in rrd_list:
        rrd_path = os.path.join(MUNIN_PATH, "{}-{}".format(machine, rrd))
        try:
            res = rrdtool.fetch(rrd_path, "AVERAGE", ["-r", "300", "-s", "{}".format(since)])
            data.append([v[0] for v in res[2]])
        except rrdtool.OperationalError:
            data.append([])
    summed_data = [sum_none(timepoint) for timepoint in itertools.zip_longest(*data)]
    summed_data = [x for x in summed_data if x != None]
    return quantile(summed_data, q)

# Given a list of RRD files, sum the metrics for each time interval,
# and compute a weighted average of the resulting serie of sums.
# Instead of summing the metrics at each time interval, another aggregation
# function can be used: max, avg...
def fetch_weighted_average(machine, rrd_list, since="-2d", aggregate_func=sum):
    data = list()
    for rrd in rrd_list:
        rrd_path = os.path.join(MUNIN_PATH, "{}-{}".format(machine, rrd))
        try:
            res = rrdtool.fetch(rrd_path, "AVERAGE", ["-r", "300", "-s", "{}".format(since)])
            data.append([v[0] for v in res[2]])
        except rrdtool.OperationalError:
            data.append([])
    aggregated_data = [map_except_none(aggregate_func, timepoint) for timepoint in itertools.zip_longest(*data)]
    # Second step to remove None results
    aggregated_data = [x for x in aggregated_data if x != None]
    return weighted_average(aggregated_data)

# Returns the sum of I/O utilization for all disks as a floating
# number (0.5 means 50%).  Can be higher than 1 if several disks are
# busy.  None if no data found.
def disk_io(machine):
    util = [fetch_avg(machine, 'diskstats_utilization-{}_util-g.rrd'.format(disk))
            for disk in DISKS]
    if all_none(util):
        return
    return sum_none(util) / 100

# Return the weighted average of I/O utilisation.
# At each time interval, we consider the max utilisation across all disks.
# Previously, we used the sum of I/O utilisation for all disks, but this does not make sense for software RAID.
def disk_io_weighted_average(machine):
    rrds = ['diskstats_utilization-{}_util-g.rrd'.format(disk)
            for disk in DISKS]
    res = fetch_weighted_average(machine, rrds, aggregate_func=max)
    if res != None:
        return res / 100

# Returns the average amount of memory usage in MiB.
def memory_usage(machine):
    util = [fetch_avg(machine, 'memory-{}-g.rrd'.format(field))
            for field in memory_fields(machine)]
    if all_none(util):
        return
    return sum_none(util) / 1024 / 1024

# Returns a quantile of memory usage, in MiB
def memory_quantile(machine, q):
    rrds = ['memory-{}-g.rrd'.format(field)
            for field in memory_fields(machine)]
    res = fetch_quantile(machine, rrds, q)
    if res != None:
        return res / 1024 / 1024

# Returns a weighted average of memory usage, in MiB
def memory_weighted_average(machine):
    rrds = ['memory-{}-g.rrd'.format(field)
            for field in memory_fields(machine)]
    res = fetch_weighted_average(machine, rrds)
    if res != None:
        return res / 1024 / 1024

# Returns the average amount of CPU usage, in percentage (200% = 2 threads used).
def cpu_usage(machine):
    util = [fetch_avg(machine, 'cpu-{}-d.rrd'.format(field))
            for field in cpu_fields(machine)]
    if all_none(util):
        return
    return sum_none(util)

# Returns a quantile of CPU usage, in percentage (200% = 2 threads used).
def cpu_quantile(machine, q):
    rrds = ['cpu-{}-d.rrd'.format(field)
            for field in cpu_fields(machine)]
    return fetch_quantile(machine, rrds, q)

# Returns a weighted average of CPU usage, in percentage (200% = 2 threads used).
def cpu_weighted_average(machine):
    rrds = ['cpu-{}-d.rrd'.format(field)
            for field in cpu_fields(machine)]
    return fetch_weighted_average(machine, rrds)



def compute_cpu_usage(machine):
    res = cpu_weighted_average(machine)
    if res == None:
        return 0
    return res

def compute_memory_usage(machine):
    res = memory_weighted_average(machine)
    if res == None:
        return 0
    return res

def compute_disk_io_usage(machine):
    res = disk_io_weighted_average(machine)
    if res == None:
        return 0
    return res
