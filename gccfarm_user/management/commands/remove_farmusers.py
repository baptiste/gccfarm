# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand, CommandError

from ansible_farm import deploy


class Command(BaseCommand):
    help = 'Remove the given Unix users on farm machines.  Runs in dry-run mode by default.'

    def add_arguments(self, parser):
        parser.add_argument('user', nargs='+',
                            help='Remove the specified user(s)')
        parser.add_argument('--machine', '-m', action='append',
                            help='Only remove users on the specified machine(s) (default: all machines)')
        parser.add_argument('--apply', action='store_true',
                            help="Actually apply the changes")

    def handle(self, *args, **options):
        if not options['apply']:
            self.stderr.write("Running in test mode\n")
        deploy.remove_users(userlist=options['user'], machinelist=options['machine'], test_mode=not options['apply'])
        if not options['apply']:
            self.stderr.write("\nTest mode completed, re-run with --apply to remove users for real")
