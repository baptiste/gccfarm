# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand, CommandError

from ansible_farm import deploy


class Command(BaseCommand):
    help = 'Create missing Unix users on farm machines.  Runs in dry-run mode by default.'

    def add_arguments(self, parser):
        parser.add_argument('--apply', action='store_true',
                            help="Actually apply the changes")

    def handle(self, *args, **options):
        deploy.deploy_latest_users(not options['apply'])
