# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

import pprint

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from gccfarm_user.models import FarmUser


class Command(BaseCommand):
    help = 'Create a new farm user, which will be immediately approved'

    def add_arguments(self, parser):
        parser.add_argument('username')
        parser.add_argument('uid', type=int)
        parser.add_argument('email')

    def handle(self, *args, **options):
        u = FarmUser(username=options['username'],
                     first_name=options['username'],
                     uid=options['uid'],
                     email=options['email'],
                     contributions="This user has been created manually using the 'create_farmuser' management command.")
        try:
            u.full_clean(exclude=['password'])
        except ValidationError as e:
            self.stderr.write(pprint.pformat(e.message_dict))
        else:
            u.save()
            self.stdout.write("Account creation successful")
