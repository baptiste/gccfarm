# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from gccfarm_user.models import FarmUser

import datetime
import os


class Command(BaseCommand):
    help = 'Import the creation date of old users (pre-Django, i.e. pre-May 2017) into the database.  Uses the creation time on the home directory.'

    def handle(self, *args, **options):
        old_home_dirs = "/home/admin-ansible/farm-ssh"
        for x in os.scandir(old_home_dirs):
            if not x.is_dir():
                continue
            user = FarmUser.objects.get(username=x.name)
            if user is None:
                self.stderr.write(f"Unknown user {x.name}")
                continue
            creation_timestamp = x.stat().st_mtime
            creation_time = datetime.datetime.fromtimestamp(creation_timestamp)
            # Use the same date for all fields.
            # Technically the request date should be earlier, but we don't know the real one.
            user.date_joined = creation_time
            user.request_date = creation_time
            user.approved_date = creation_time
            user.save()
            msg = f"Updated user {x.name}"
            self.stdout.write(self.style.SUCCESS(msg))
