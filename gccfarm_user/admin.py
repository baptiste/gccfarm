# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.db import models
from easy_select2.widgets import Select2Multiple, Select2

from .models import FarmUser, SSHKeyDeployQueue, SSHKeyDeployBacklog


farmuser_fieldsets = list(UserAdmin.fieldsets)
farmuser_fieldsets.insert(2, ('GCC Farm',
                              {'fields':
                               ('uid', 'contributions', 'deployment_started', 'request_date', 'approved', 'reject_reason', 'approved_by', 'approved_date')}))

class FarmUserAdmin(UserAdmin):
    list_display = ('username', 'uid', 'email', 'name', 'request_date', 'approved', 'approved_by', 'approved_date', 'is_active')
    list_filter = ('approved', 'deployment_started', 'is_active', 'is_superuser', 'groups')
    search_fields = ['username', 'first_name', 'last_name', 'email', 'contributions']
    readonly_fields = ('deployment_started', 'request_date', 'approved', 'approved_by', 'approved_date', 'reject_reason')
    ordering = ('-uid', '-request_date')
    fieldsets = farmuser_fieldsets

    def name(self, obj):
        return '{} {}'.format(obj.first_name, obj.last_name).strip()
    name.admin_order_field = 'first_name'


class SSHKeyDeployQueueAdmin(admin.ModelAdmin):
    list_display = ('user', 'added')

    formfield_overrides = {
        models.ManyToManyField: {'widget': Select2Multiple},
        models.ForeignKey: {'widget': Select2},
    }


class SSHKeyDeployBacklogAdmin(admin.ModelAdmin):
    list_display = ('user', 'machine', 'added')

    formfield_overrides = {
        models.ManyToManyField: {'widget': Select2Multiple},
        models.ForeignKey: {'widget': Select2},
    }



admin.site.register(FarmUser, FarmUserAdmin)
admin.site.register(SSHKeyDeployQueue, SSHKeyDeployQueueAdmin)
admin.site.register(SSHKeyDeployBacklog, SSHKeyDeployBacklogAdmin)
