# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .models import SSHKeyDeployQueue


def add_user_to_sshkey_deploy_queue(sender, instance, **kwargs):
    """[instance] is a simplesshkey.models.UserKey object"""
    queueitem = SSHKeyDeployQueue(user=instance.user)
    queueitem.save()
