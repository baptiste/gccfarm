# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Article


class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'published', 'pub_date']
    list_filter = ['published', 'pub_date']
    date_hierarchy = 'pub_date'

admin.site.register(Article, ArticleAdmin)
