# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date

from django.db import models

from ckeditor.fields import RichTextField


class PublishedArticleManager(models.Manager):
    """Manager that only returns "shared" machines (that is, those with
    shared=True and retired=False).  This manager should be used when
    presenting information to users.
    """
    def get_queryset(self):
        return super(PublishedArticleManager, self).get_queryset().filter(published=True)

class Article(models.Model):
    title = models.CharField(max_length=256, verbose_name='title')
    pub_date = models.DateField(verbose_name='publication date',
                                default=date.today)
    published = models.BooleanField(default=True, verbose_name='published?')
    content = RichTextField(blank=True, verbose_name='content')

    objects = models.Manager()
    articles = PublishedArticleManager()

    class Meta:
        ordering = ['-pub_date', '-id']
