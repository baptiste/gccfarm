# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-07-10 20:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0003_auto_20200710_2045'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hostercontact',
            options={'ordering': ['hoster', '-last_updated']},
        ),
        migrations.AlterModelOptions(
            name='projectcontact',
            options={'ordering': ['project', '-last_updated']},
        ),
    ]
