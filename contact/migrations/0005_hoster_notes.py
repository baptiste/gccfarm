# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-05-19 21:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0004_auto_20200710_2058'),
    ]

    operations = [
        migrations.AddField(
            model_name='hoster',
            name='notes',
            field=models.TextField(blank=True, help_text='Additional private information: details of relationship, technical setup, IP addresses, admin access...', verbose_name='notes'),
        ),
    ]
