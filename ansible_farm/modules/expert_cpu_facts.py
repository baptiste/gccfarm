#!/usr/bin/python

# Copyright: (c) 2022, Baptiste Jonglez <whatever@bitsofnetworks.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: expert_cpu_facts

short_description: Collects advanced CPU facts from hwloc and lscpu

version_added: "1.0.0"

description: This module is useful when the basic processor facts provided
by Ansible are not sufficient or are incorrect.  This is often the case on
non-x86_64 hardware, because Ansible parses /proc/cpuinfo (on Linux) which
is very brittle.  This module collects data from hwloc and lscpu
independently, and lets the user decide which piece of data is useful for
the task at hand.  If hwloc or lscpu is not available on the target
system, the relevant facts will not be collected.

author:
    - Baptiste Jonglez (@jonglezb)
'''

EXAMPLES = r'''
'''

RETURN = r'''
'''

import json

from ansible.module_utils.basic import AnsibleModule


def parse_lscpu_text(module, data):
    res = list()
    for line in data.splitlines():
        data = line.split(":", 1)
        key = data[0].strip()
        value = data[1].strip()
        res.append((key, value))
    return res

def collect_lscpu_facts(module):
    empty_facts = []
    lscpu = module.get_bin_path('lscpu')
    if not lscpu:
        return empty_facts
    rc, out, err = module.run_command(["lscpu"])
    if rc != 0:
        return empty_facts
    return parse_lscpu_text(module, out)


def collect_hwloc_facts(module):
    facts = dict()
    hwloc = module.get_bin_path('hwloc-calc')
    if not hwloc:
        return facts
    for item in ("machine", "numanode", "package", "core", "pu"):
        rc, out, err = module.run_command(["hwloc-calc", "-N", item, "all"])
        if rc == 0:
            count = None
            try:
                count = int(out.strip())
            except ValueError:
                pass
            facts[item] = count
    return facts

def run_module():

    result = dict(
        ansible_facts=dict(),
    )

    module = AnsibleModule(
        argument_spec=dict(),
        supports_check_mode=True
    )

    result['ansible_facts']['lscpu'] = collect_lscpu_facts(module)
    result['ansible_facts']['hwloc'] = collect_hwloc_facts(module)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()

