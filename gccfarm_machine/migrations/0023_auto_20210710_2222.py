# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-07-10 22:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gccfarm_machine', '0022_auto_20210321_1036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='machine',
            name='nb_cores',
            field=models.PositiveIntegerField(default=0, help_text='Total number of CPU cores', verbose_name='CPU cores'),
        ),
        migrations.AlterField(
            model_name='machine',
            name='nb_cpu',
            field=models.PositiveIntegerField(default=0, help_text='Number of CPU sockets or packages. Only used on x86 and if not 0.', verbose_name='CPU sockets'),
        ),
        migrations.AlterField(
            model_name='machine',
            name='nb_threads',
            field=models.PositiveIntegerField(default=0, help_text='Total number of CPU threads', verbose_name='CPU threads'),
        ),
    ]
