# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import path

from gccfarm_machine import views


urlpatterns = [
    path('list/', views.MachineList.as_view(), name='platform'),
]
