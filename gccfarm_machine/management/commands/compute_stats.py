# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division

from django.core.management.base import BaseCommand, CommandError
import django.utils.timezone

from gccfarm_machine.models import Machine
import rrd_stats


class Command(BaseCommand):
    help = 'Compute statistics on the usage of each machine (from munin) and store it in the database'

    def add_arguments(self, parser):
        parser.add_argument('hostname', nargs='*',
                            help='Only compute statistics for the specified machine(s) (default: all machines)')
        parser.add_argument('--quiet', '-q', action='store_true',
                            help="Don't display anything (useful when running from cron)")

    def handle(self, *args, **options):
        if options['hostname'] == None or options['hostname'] == []:
            machines = Machine.objects.all()
        else:
            machines = Machine.objects.filter(name__in=options['hostname'])
        for m in machines:
            if m.nb_threads == 0:
                m.cpu_usage = 0
            else:
                m.cpu_usage = min(rrd_stats.compute_cpu_usage(m.name) / 100 / m.nb_threads, 1)
            if m.memtotal_mb == 0:
                m.memory_usage = 0
            else:
                m.memory_usage = min(rrd_stats.compute_memory_usage(m.name) / m.memtotal_mb, 1)
            # Disk IO is already a ratio, but can be greater than 1 if there are multiple disks
            m.disk_io_usage = min(rrd_stats.compute_disk_io_usage(m.name), 1)
            m.last_stats = django.utils.timezone.now()
            m.save()
