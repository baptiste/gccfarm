# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand, CommandError

from ansible_farm import machines


class Command(BaseCommand):
    help = 'Gather information about farm machines and store it in the database'

    def add_arguments(self, parser):
        parser.add_argument('hostname', nargs='*',
                            help='Only gather information for the specified machine(s) (default: all machines)')
        parser.add_argument('--include-retired', '-r', action='store_true',
                            help="Also gather information from retired machines")
        parser.add_argument('--quiet', '-q', action='store_true',
                            help="Don't display anything (useful when running from cron)")

    def handle(self, *args, **options):
        machines.gather_info(options['hostname'], options['include_retired'], options['quiet'])
