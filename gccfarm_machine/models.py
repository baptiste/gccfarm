# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from itertools import groupby
import re

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.core.validators import validate_comma_separated_integer_list

from contact.models import Hoster, Project


class ManagedMachineManager(models.Manager):
    """Manager that only returns "managed" machines (that is, those with
    managed_users=True and retired=False).  This manager should be used
    for any action related to automated deployment of farm users and SSH
    keys.
    """
    def get_queryset(self):
        return super(ManagedMachineManager, self).get_queryset().filter(managed_users=True,
                                                                        retired=False)


class SharedMachineManager(models.Manager):
    """Manager that only returns "shared" machines (that is, those with
    shared=True and retired=False).  This manager should be used when
    presenting information to users.
    """
    def get_queryset(self):
        return super(SharedMachineManager, self).get_queryset().filter(shared=True,
                                                                       retired=False)


class Machine(models.Model):
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    # Manually-managed fields
    name = models.CharField(max_length=64, unique=True,
                            help_text='Short name of the machine')
    hostname = models.CharField(max_length=255,
                                help_text='Fully qualified hostname used to connect to the machine')
    ssh_ports = models.CharField(default='22', max_length=128,
                                 verbose_name='SSH ports',
                                 help_text='Comma-separated list of TCP ports for SSH',
                                 validators=[validate_comma_separated_integer_list])
    ssh_admin_user = models.CharField(default='root', max_length=64,
                                 verbose_name='SSH admin username',
                                 help_text='Used to run ansible tasks')
    ssh_configuration = models.TextField(null=False, blank=True,
                                         verbose_name='SSH configuration',
                                         help_text='Additional SSH client configuration, only displayed to users')
    python_interpreter = models.CharField(default='/usr/bin/python3', max_length=64,
                                          verbose_name='python interpreter',
                                          help_text='Full path to python interpreter used by ansible')
    home_directory = models.CharField(default='/home', max_length=64,
                                          verbose_name='home directory',
                                          help_text='Root path of home directories')
    description = models.CharField(max_length=255, blank=True,
                                   help_text='Short summary of the specificities or purpose of the machine')
    hoster = models.ForeignKey(Hoster, on_delete=models.SET_NULL, blank=True, null=True,
                               help_text='Organisation hosting the machine',
                               related_name='machines')
    location = models.CharField(max_length=255, blank=True,
                                help_text='Physical location (country, city) of the machine')
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, blank=True, null=True,
                                help_text='Project using the machine exclusively',
                                related_name='machines')
    largest_deployed_uid = models.IntegerField(default=0, verbose_name='largest deployed UID',
                                               help_text='Largest user UID deployed on this machine (without offset), used to determine which users still need be deployed.  Change this with caution!')
    uid_offset = models.IntegerField(default=60000, verbose_name='UID offset',
                                     help_text='Offset applied to the UID of deployed users (e.g. UID=1042,offset=1000 => effective deployed UID is 2042')
    # Only related to visibility
    shared = models.BooleanField(default=True, verbose_name='shared',
                                 help_text='Display the machine to users')
    # Only related to user deployment (some shared machines create users in another way, such as NFS)
    managed_users = models.BooleanField(default=True, verbose_name='managed users',
                                        help_text='Whether the machine should receive automated user deployments')
    retired = models.BooleanField(default=False, verbose_name='retired',
                                  help_text='Retired machines have very little chance of coming back in the farm')
    retired_since = models.DateTimeField(null=True, editable=False, verbose_name='retired since',
                                         help_text='Date of retirement')

    # Fields that are automatically filled by Ansible
    # Metadata
    success = models.BooleanField(default=False, editable=False, verbose_name='reachable',
                                  help_text='Whether the last gathering process was successful')
    last_success = models.DateTimeField(null=True, editable=False, verbose_name='last reachable',
                                        help_text='Date of the last successful gathering process')
    manual_cpu_count = models.BooleanField(default=False,
                                           help_text='Manage the CPU/cores/threads count manually instead of trusting ansible')
    # These fields are editable because sometimes Ansible gets it wrong
    nb_cpu = models.PositiveIntegerField(default=0, editable=True, verbose_name='CPU sockets',
                                         help_text="Number of CPU sockets or packages. Only used on x86 and if not 0.")
    nb_cores = models.PositiveIntegerField(default=0, editable=True, verbose_name='CPU cores',
                                           help_text="Total number of CPU cores")
    nb_threads = models.PositiveIntegerField(default=0, editable=True, verbose_name='CPU threads',
                                             help_text="Total number of CPU threads")
    processor = models.CharField(max_length=512, blank=True, editable=True,
                                 help_text="Will be overwritten if found on the system")
    # The rest of the technical data is not editable
    architecture = models.CharField(max_length=512, blank=True, editable=False)
    kernel = models.CharField(max_length=512, blank=True, editable=False)
    os = models.CharField(max_length=255, blank=True, editable=False)
    memtotal_mb = models.BigIntegerField(default=0, editable=False)
    swaptotal_mb = models.BigIntegerField(default=0, editable=False)
    mount_root_fstype = models.CharField(max_length=64, blank=True, editable=False)
    mount_root_size_total = models.BigIntegerField(default=0, editable=False,
                                                   help_text='In bytes')
    mount_root_size_available = models.BigIntegerField(default=0, editable=False,
                                                       help_text='In bytes')
    mount_home_fstype = models.CharField(max_length=64, blank=True, editable=False)
    mount_home_size_total = models.BigIntegerField(default=0, editable=False,
                                                   help_text='In bytes')
    mount_home_size_available = models.BigIntegerField(default=0, editable=False,
                                                       help_text='In bytes')
    uptime = models.DurationField(default=datetime.timedelta, editable=False)
    # Statistics that are automatically filled thanks to munin
    last_stats = models.DateTimeField(null=True, editable=False,
                                      help_text='Date of the last successful statistics computation')
    # Actual statistical data, each between 0 and 1.  They can be
    # interpreted as a percentage, although the exact calculation of each
    # metric is unspecified.
    cpu_usage = models.FloatField(default=0., editable=False)
    memory_usage = models.FloatField(default=0., editable=False)
    disk_io_usage = models.FloatField(default=0., editable=False)

    # Custom manager in addition to the default manager
    objects = models.Manager()
    managed_machines = ManagedMachineManager()
    shared_machines = SharedMachineManager()

    def numeric_name(self):
        match = re.search(r'\d+', self.name)
        if match is not None:
            return int(match[0])
        return 0

    def unreachable_since(self):
        if self.success:
            return datetime.timedelta(0)
        if not self.last_success:
            return None
        return timezone.now() - self.last_success

    def ssh_ports_list(self):
        return [int(port) for port in self.ssh_ports.split(',')]

    def first_ssh_port(self):
        if self.ssh_ports != "":
            return sorted(self.ssh_ports_list())[0]

    def hostname_port(self):
        return '{}:{}'.format(self.hostname, self.first_ssh_port())

    def graph_url(self, category):
        return settings.CFARM_MACHINE_GRAPHS_LINK[category].format(name=self.name)

    def graph_url_overview(self):
        return self.graph_url('overview')

    def graph_url_cpu(self):
        return self.graph_url('cpu')

    def graph_url_memory(self):
        return self.graph_url('memory')

    def graph_url_disk(self):
        return self.graph_url('disk')

    def graph_url_disk_io(self):
        return self.graph_url('disk_io')

    def mount_size_total(self):
        if self.mount_home_size_total > 0:
            return self.mount_home_size_total
        else:
            return self.mount_root_size_total

    def mount_fstype(self):
        if self.mount_home_size_total > 0:
            return self.mount_home_fstype
        else:
            return self.mount_root_fstype

    def memtotal(self):
        return self.memtotal_mb * 1024 * 1024

    def save(self, *args, **kwargs):
        # Check if "retired" has just been enabled. We need to fetch the
        # object from the database to compare.
        if self.pk:
            old_machine = Machine.objects.get(pk=self.pk)
            if not old_machine.retired and self.retired:
                self.retired_since = timezone.now()
                # Cleanup any remaining queued deployment tasks for this machine
                self.sshkey_deploy_backlog.all().delete()
        if not self.retired:
            # Cleanup date of retirement
            self.retired_since = None
        super(Machine, self).save(*args, **kwargs)

    @classmethod
    def ssh_config(cls):
        """Generate a .ssh/config extract for shared farm machines. It includes
        the SSH port (if different from 22) as well as any additional
        necessary client config such as ciphers or key exchange algorithms.
        """
        # Only include machines with specific config
        machines = sorted([m for m in cls.shared_machines.all()
                           if (m.ssh_ports != "" and 22 not in m.ssh_ports_list())
                           or (m.ssh_configuration != "")
                           ],
                          key=lambda m: m.numeric_name())
        if len(machines) == 0:
            return ""
        output = list()
        for machine in machines:
            s = "Host {}\n".format(machine.hostname)
            port = machine.first_ssh_port()
            if port and port != 22:
                s += "    Port {}\n".format(port)
            for line in machine.ssh_configuration.split("\n"):
                config = line.strip()
                if config:
                    s += "    {}\n".format(config)
            output.append(s)
        return '\n'.join(output)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
