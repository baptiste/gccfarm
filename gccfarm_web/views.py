from django.shortcuts import render
from django.views.generic import DetailView, ListView

from gccfarm_news.models import Article


class PublishedArticlesMixin(object):
    def get_queryset(self):
        return self.model.articles.all()


class HomeView(PublishedArticlesMixin, ListView):
    """The home view is a simple (static) template, but we also display the latest news"""
    model = Article
    template_name = 'gccfarm_web/home.html'
    context_object_name = 'articles'

    def get_queryset(self):
        return super(HomeView, self).get_queryset()[:3]

class NewsView(PublishedArticlesMixin, ListView):
    model = Article
    template_name = 'gccfarm_web/news.html'
    context_object_name = 'articles'
    paginate_by = 10

class NewsDetailView(PublishedArticlesMixin, DetailView):
    model = Article
    template_name = 'gccfarm_web/news_detail.html'
    context_object_name = 'article'


def rules_hosts(request):
    return render(request, 'gccfarm_web/rules_hosts.html')

def join_confirmation(request):
    return render(request, 'gccfarm_web/join_confirmation.html')

def tickets(request):
    return render(request, 'gccfarm_web/tickets.html')

def user_profile(request):
    return render(request, 'gccfarm_web/user_profile.html')

def manage_machines(request):
    return render(request, 'gccfarm_web/manage_machines.html')

def manage_groups(request):
    return render(request, 'gccfarm_web/manage_groups.html')

def see_users(request):
    return render(request, 'gccfarm_web/see_users.html')
