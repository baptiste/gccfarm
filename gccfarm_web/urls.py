from django.urls import include, path, re_path

from gccfarm_web import views
from gccfarm_news import feeds
import simplesshkey.views

sshkey_patterns = ([
    path('create/', simplesshkey.views.userkey_add, name='userkey_add'),
    path('list/', simplesshkey.views.userkey_list, name='userkey_list'),
    re_path(r'^(?P<pk>\d+)/delete$', simplesshkey.views.userkey_delete, name='userkey_delete'),
], 'simplesshkey')

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('rss/', feeds.RSS(), name='rss'),
    re_path(r'^news/(?P<pk>\d+)', views.NewsDetailView.as_view(), name='news_detail'),
    path('news/', views.NewsView.as_view(), name='news'),
    path('rules/hosts', views.rules_hosts, name='rules_hosts'),
    path('tickets/', views.tickets, name='tickets'),
    path('profile/', views.user_profile, name='user_profile'),
    path('join/done/', views.join_confirmation, name='join_confirmation'),
    #path('machines/', views.manage_machines, name='manage_machines'),
    #path('groups/', views.manage_groups, name='manage_groups'),
    #path('users/', views.see_users, name='see_users'),
    path('sshkey/', include(sshkey_patterns)),
]
